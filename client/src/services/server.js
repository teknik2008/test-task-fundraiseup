import { URL } from 'url'
import { postJson } from '../http/index.js'
import * as HTTPErrors from '../http/errors.js'
import Logger from '../../../common/logger.js'
import { ExponentialBackoff } from '../util/exponential-backoff.js'
import { SERVER_URL } from '../../../common/constants.js'

const logger = new Logger('client:server')
const serverBase = SERVER_URL

const StatisticsRequests = (function () {
  const errorAmounts = {
    total: 0,
    success: 0,
    serverError: 0,
    timeOut: 0,
    anyError: 0
  }

  function requestSuccess () {
    errorAmounts.total++
    errorAmounts.success++
  }

  function requestError (error) {
    errorAmounts.total++
    if (error instanceof HTTPErrors.ETIMEDOUT) {
      errorAmounts.timeOut++
    } else if (error instanceof HTTPErrors.ERR_BAD_RESPONSE) {
      errorAmounts.serverError++
    } else {
      errorAmounts.anyError++
    }
  }

  function printStatistic () {
    logger.info(`
    requests:
      total: ${errorAmounts.total}
      success: ${errorAmounts.success}
      timeOut: ${errorAmounts.timeOut}
      serverError: ${errorAmounts.serverError}
    `)
  }

  return {
    requestSuccess,
    requestError,
    printStatistic
  }
})()

export async function emitPing (pingData) {
  let deliveryAttempt = 1
  const urlPath = '/data'
  const uri = (new URL(urlPath, serverBase)).toString()
  const postJsonEB = new ExponentialBackoff(postJson, uri, pingData)
  postJsonEB.on('error', (error) => {
    deliveryAttempt++
    pingData.deliveryAttempt = deliveryAttempt
    postJsonEB.updateArgs(uri, pingData)
    StatisticsRequests.requestError(error)
    logger.info('error send request to server: ' + error.message)
  })
  postJsonEB.on('beforeSend', (...args) => {
    logger.info('before send request to server ' + JSON.stringify(args))
  })

  StatisticsRequests.requestSuccess()
  return postJsonEB.call()
}

function exitHandler () {
  StatisticsRequests.printStatistic()
  process.exit()
}

process.on('SIGINT', exitHandler)
process.on('SIGUSR1', exitHandler)
process.on('SIGUSR2', exitHandler)
process.on('message', (message) => {
  if (message.command === 'SIGINT') exitHandler()
})
