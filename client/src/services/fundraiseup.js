import { get } from '../http/index.js'
import { performance } from 'perf_hooks'
import { URL } from 'url'
const fundraiseupUrlBase = 'https://fundraiseup.com'

export async function timing (urlPath = '/') {
  const uri = new URL(urlPath, fundraiseupUrlBase)
  const start = performance.now()
  await get(uri.toString())
  const end = performance.now()
  return end - start
}
