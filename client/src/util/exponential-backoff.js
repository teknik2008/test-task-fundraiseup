
import EventEmitter from 'events'
import Logger from '../../../common/logger.js'

const logger = new Logger('ExponentialBackoff')

export class ExponentialBackoff extends EventEmitter {
  /**
   *
   * @param {Promise} fnPr
   */
  constructor (fnPr, ...args) {
    super()
    this.fnPr = fnPr
    this.minDelay = 10
    this.maxDelay = 1000 * 60 * 1
    this.factor = 2
    this.delay = this.minDelay
    this.fnArgs = args
  }

  /**
   *
   * @param  {...any} args
   */
  async call () {
    try {
      this.emit('beforeSend', ...this.fnArgs)
      const result = await this.fnPr(...this.fnArgs)
      this.resetRetry()
      return result
    } catch (error) {
      this.emit('error', error)
      await this.sleep(this.delay)
      this.updateRetry()
      return this.call()
    }
  }

  updateArgs (...args) {
    this.fnArgs = args
  }

  updateRetry () {
    this.delay += (this.delay)
    this.delay = Math.min(this.delay, this.maxDelay)
    logger.debug(`next delay ${this.delay}ms`)
  }

  resetRetry () {
    this.delay = this.minDelay
  }

  sleep (delay) {
    return new Promise((resolve) => { setTimeout(resolve, delay) })
  }
}
