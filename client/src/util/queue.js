import EventEmitter from 'events'

export class Queue extends EventEmitter {
  constructor () {
    super()
    this.elements = {}
    this.head = 0
    this.tail = 0
  }

  enqueue (element) {
    this.elements[this.tail] = element
    this.tail++
    this.emit('enqueue')
  }

  dequeue () {
    const item = this.elements[this.head]
    delete this.elements[this.head]
    this.head++
    return item
  }

  peek () {
    return this.elements[this.head]
  }

  get length () {
    return this.tail - this.head
  }

  get isEmpty () {
    return this.length === 0
  }

  waitNextItem () {
    if (this.length > 0) return Promise.resolve()
    return new Promise(resolve => {
      this.once('enqueue', resolve)
    })
  }

  [Symbol.asyncIterator] () {
    return {
      next: async () => {
        await this.waitNextItem()
        const value = this.dequeue()
        return { value, done: false }
      }
    }
  }
}
