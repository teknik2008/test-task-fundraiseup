import https from 'https'
import http from 'http'
import { URL } from 'url'
import { ETIMEDOUT, ERR_NETWORK, ERR_BAD_OPTIONS, ERR_BAD_RESPONSE } from './errors.js'
import { STATUS_CODES } from '../../../common/constants.js'

const Timeout = 10 * 1000

export function get (uri, headers) {
  const options = { method: 'GET', headers, timeout: Timeout }
  return request(uri, options)
}

export function post (uri, body, headers = {}) {
  const options = { method: 'POST', headers, timeout: Timeout }
  return request(uri, options, body)
}

export async function postJson (uri, json, headers = {}) {
  const body = JSON.stringify(json)
  headers['content-type'] = 'application/json'
  const options = { method: 'POST', headers, timeout: Timeout }
  return request(uri, options, body)
}

function request (uri, options, body) {
  return new Promise((resolve, reject) => {
    const uriOptions = new URL(uri)
    const transport = getHttpTransportForUrlProtocol(uriOptions.protocol)

    const request = transport.request(uri, options, async (res) => {
      const statusCode = res.statusCode
      const bodyRaw = []
      const headers = res.headers
      for await (const chunk of res) {
        bodyRaw.push(chunk)
      }
      const body = await parseResponse(bodyRaw, headers, statusCode).catch(reject)
      if (statusCode >= STATUS_CODES.OK && statusCode < STATUS_CODES.MULTIPLE_CHOICE) {
        return resolve({ body, headers, statusCode })
      }
      reject(new ERR_BAD_RESPONSE('error status code', statusCode, body, headers))
    })

    request.on('error', (error) => {
      reject(new ERR_NETWORK(error))
    })

    request.on('timeout', function () {
      request.end()
      reject(new ETIMEDOUT('timeout'))
    })

    if (body)request.write(body)
    request.end()
  })
}

async function parseResponse (bodyRaw, headers, statusCode) {
  const body = bodyRaw.toString()
  const contentType = (headers['content-type'] || '').split(';')[0].toLowerCase()
  if (contentType === 'application/json') {
    try {
      return JSON.parse(body)
    } catch (error) {
      throw new ERR_BAD_RESPONSE(error.message, statusCode, body.toString(), headers)
    }
  }
  return body
}

function getHttpTransportForUrlProtocol (protocol) {
  if (protocol === 'https:') { return https }
  if (protocol === 'http:') { return http }
  throw new ERR_BAD_OPTIONS(`protocol ${protocol} not supported`)
}
