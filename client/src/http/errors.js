export class HTTP_ERROR extends Error {}

export class ETIMEDOUT extends HTTP_ERROR {
  constructor (message = 'timeout') {
    super(message)
  }
}

export class ERR_BAD_OPTIONS extends HTTP_ERROR {
  constructor (message = 'bad request') {
    super(message)
  }
}

export class ERR_BAD_RESPONSE extends HTTP_ERROR {
  constructor (message = 'bad request', statusCode, body, headers) {
    super(message)
    this.statusCode = statusCode
    this.body = body
    this.headers = headers
  }
}
export class ERR_NETWORK extends HTTP_ERROR {
  constructor (message = 'network error') {
    super(message)
  }
}
