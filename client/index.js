import { Queue } from './src/util/queue.js'
import * as Server from './src/services/server.js'
import * as Fundraiseup from './src/services/fundraiseup.js'
import Logger from '../common/logger.js'

const logger = new Logger('client')

const queue = new Queue()

async function main () {
  let pingId = 0

  const timer = setInterval(async () => {
    try {
      const responseTime = await Fundraiseup.timing()
      const date = Date.now()
      const deliveryAttempt = 1
      queue.enqueue({ responseTime, date, pingId, deliveryAttempt })
      pingId++
    } catch (error) {
      logger.error('error add ping to queue' + error.message)
    }
  }, 1000)
  try {
    for await (const item of queue) {
      await Server.emitPing(item)
    }
  } catch (error) {
    clearInterval(timer)
    throw error
  }
}

main().catch(error => {
  logger.error(error)
})
