import { average, median } from '../util/aggregate.js'
import Logger from '../../../common/logger.js'

const logger = new Logger('statistic')

class PingStatistic {
  constructor () {
    this.responseTimeList = []
  }

  addResponseTime (responseTime) {
    this.responseTimeList.push(responseTime)
  }

  /**
   *
   * @returns {number}
   */
  avgResponseTime () {
    return average(this.responseTimeList)
  }

  /**
   *
   * @returns {number}
   */
  medianResponseTime () {
    return median(this.responseTimeList)
  }
}

const pingStatistic = new PingStatistic()

function exitHandler () {
  logger.info(`avg: ${pingStatistic.avgResponseTime()}, median: ${pingStatistic.medianResponseTime()}`)
  process.exit()
}

process.on('SIGINT', exitHandler)
process.on('SIGUSR1', exitHandler)
process.on('SIGUSR2', exitHandler)
process.on('message', (message) => {
  if (message.command === 'SIGINT') exitHandler()
})

export default pingStatistic
