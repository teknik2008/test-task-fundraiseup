import { parseBody } from '../util/parse-body.js'
import { parseTryJson } from '../util/parse-try-json.js'
import PingStatistic from '../repository/statistic.js'

export const ping = async function ping (req, res) {
  const bodyString = await parseBody(req)
  const body = parseTryJson(bodyString, null)
  PingStatistic.addResponseTime(body.responseTime)
  res.writeHead(200, { 'Content-type': 'application/json' })
  res.end(JSON.stringify(body))
}
