import Logger from '../../../common/logger.js'

const logger = new Logger('parse:json')

export function parseTryJson (jsonString, defaultValue = null) {
  try {
    return JSON.parse(jsonString)
  } catch (error) {
    logger.error(`Error parsing json ${error.message}`)
    return defaultValue
  }
}
