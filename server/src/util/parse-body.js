export function parseBody (req) {
  return new Promise((resolve, reject) => {
    const bodyChunks = []
    req
      .on('data', function (chunk) {
        bodyChunks.push(chunk)
      })
      .on('end', function () {
        resolve(Buffer.concat(bodyChunks).toString())
      })
      .on('error', reject)
  })
}
