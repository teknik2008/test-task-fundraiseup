/**
 *
 * @param {number[]} arr
 * @returns {number}
 */
export const median = (numbers) => {
  const sorted = numbers.slice().sort((a, b) => a - b)
  const middle = Math.floor(sorted.length / 2)

  if (sorted.length % 2 === 0) {
    return (sorted[middle - 1] + sorted[middle]) / 2
  }

  return sorted[middle]
}

/**
 *
 * @param {number[]} arr
 * @returns {number}
 */
export const average = arr => arr.reduce((p, c) => p + c, 0) / arr.length
