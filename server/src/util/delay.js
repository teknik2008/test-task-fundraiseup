/**
 *
 * @param {number} delayTime
 * @returns {Promise<void>}
 */
export const delay = (delayTime) => {
  const delayTimeMs = delayTime * 1000
  return new Promise((resolve) => setTimeout(resolve, delayTimeMs))
}
