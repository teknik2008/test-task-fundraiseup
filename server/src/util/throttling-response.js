import { delay } from './delay.js'
import Logger from '../../../common/logger.js'
import { THROTTLING_STATE } from '../../../common/constants.js'
import { InternalError } from '../../../common/errors.js'

const logger = new Logger('throttling-response')

function getThrottlingMode () {
  let state = THROTTLING_STATE.NORMAL
  setInterval(() => {
    const randomInt = Math.random()
    state = nextState(randomInt)
    logger.debug('getThrottlingMode change state to ' + state)
  }, 1000)
  return () => state
}

function nextState (randomInt) {
  if (randomInt < 0.6) return THROTTLING_STATE.NORMAL
  if (randomInt >= 0.6 && randomInt < 0.8) return THROTTLING_STATE.ERROR
  return THROTTLING_STATE.DELAY
}

const throttlingMode = getThrottlingMode()

export default async function throttlingResponse () {
  switch (throttlingMode()) {
    case THROTTLING_STATE.NORMAL:
      return
    case THROTTLING_STATE.DELAY:
      await delay(10)
      throw new InternalError('')
  }
  throw new InternalError('internal error')
}
