import { ServerError, BadRequest } from '../../../common/errors.js'

export function errorResponse (req, res, error) {
  if (!(error instanceof ServerError)) {
    error = new BadRequest(error)
  }
  const contentType = error.contentType
  const statusCode = error.statusCode
  const message = error.message
  res.writeHead(statusCode, { 'Content-type': contentType })
  res.end(message)
}
