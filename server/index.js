// server.js

import { createServer } from 'http'
import Logger from '../common/logger.js'
import { ping } from './src/controller/ping.js'
import throttlingResponse from './src/util/throttling-response.js'
import { SERVER_PORT } from '../common/constants.js'
import { errorResponse } from './src/util/error-response.js'
import { NotFound } from '../common/errors.js'

const logger = new Logger('server')

const server = createServer(async function (req, res) {
  throttlingResponse()
    .then(() => router(req, res))
    .catch((error) => errorResponse(req, res, error))
})

server.listen(SERVER_PORT, function () {
  logger.debug('Server is running at 127.0.0.1:' + SERVER_PORT)
})

async function router (req, res) {
  const method = req.method
  const [urlPath] = req.url.toLowerCase().split('?')
  switch (true) {
    case (method === 'POST' && urlPath === '/data'):
      logger.info('request POST /data')
      return ping(req, res)
  }
  throw new NotFound('not found')
}
