export const CONTENT_TYPE = {
  TEXT: 'text/plain',
  JSON: 'application/json'
}

export const THROTTLING_STATE = {
  NORMAL: 1,
  ERROR: 2,
  DELAY: 3
}

export const STATUS_CODES = {
  OK: 200,
  MULTIPLE_CHOICE: 300,
  BAD_REQUEST: 400,
  NOT_FOUND: 404,
  INTERNAL_SERVER_ERROR: 500
}

export const SERVER_PORT = parseInt(process.env.SERVER_PORT, 10) || 8080

export const SERVER_URL = process.env.SERVER_URL || `http://localhost:${SERVER_PORT}`

export const LOG_LEVEL = (process.env.LOG_LEVEL || 'info').split(',')
