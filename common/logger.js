import { LOG_LEVEL } from './constants.js'

class Logger {
  constructor (nameSpace) {
    this.nameSpace = nameSpace
  }

  info () {}
  debug () {}
  warn () {}
  error () {}
}

const logMethods = {
  info (...args) {
    console.info(this.nameSpace, ...args)
  },

  debug (...args) {
    console.debug(this.nameSpace, ...args)
  },

  warn (...args) {
    console.warn(this.nameSpace, ...args)
  },

  error (...args) {
    console.error(this.nameSpace, ...args)
  }
}

for (const logMethodName in logMethods) {
  const isIncludeMethod = LOG_LEVEL.includes(logMethodName)
  if (!isIncludeMethod) continue
  const method = logMethods[logMethodName]
  Logger.prototype[logMethodName] = method
}

export default Logger
