import { CONTENT_TYPE, STATUS_CODES } from './constants.js'

export class ServerError extends Error {}

export class NotFound extends ServerError {
  get statusCode () { return STATUS_CODES.NOT_FOUND }
  get contentType () { return CONTENT_TYPE.TEXT }
}

export class InternalError extends ServerError {
  get statusCode () { return STATUS_CODES.INTERNAL_SERVER_ERROR }
  get contentType () { return CONTENT_TYPE.TEXT }
}

export class BadRequest extends ServerError {
  get statusCode () { return STATUS_CODES.BAD_REQUEST }
  get contentType () { return CONTENT_TYPE.TEXT }
}
