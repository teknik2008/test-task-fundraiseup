import { fork } from 'child_process'

function lift () {
  const server = fork('./server/index.js')
  const client = fork('./client/index.js')
  process.on('SIGINT', exitHandler)
  process.on('SIGUSR1', exitHandler)
  process.on('SIGUSR2', exitHandler)
  function exitHandler () {
    server.send({ command: 'SIGINT' })
    client.send({ command: 'SIGINT' })
  }
}

lift()
