# Backend test

## Требования

node >=18.0.0

## Запуск

### Запуск одной командой клиент + сервер

``
npm run start
``

### Запуск клиента и сервера по отдельности

Запуск сервера

``
npm run start:server
``

Запуск клиента

``
npm run start:client
``

Запуск через docker-compose

``
docker-compose up
``